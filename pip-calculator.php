<!DOCTYPE html>
  <html>
    <head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.js"></script>
	<script type="text/javascript">
		function buyPriceAjax(){
			var xhttp;
			xhttp = new XMLHttpRequest();

			var from = document.getElementById('from').value;
			var to = document.getElementById('to').value;
			var from_amount = document.getElementById('tradesize').value;

			xhttp.onreadystatechange = function() {
				if(this.readyState == 4 && this.status == 200){

          document.getElementById("to_amount").value = this.responseText;
				}
			};
			xhttp.open("GET", "includes/pip-helper.php?function=0&from="+from+"&to="+to+"&from_amount="+from_amount, true);
			xhttp.send();
		}
	</script>
    </head>
    <body>
    <div class="container calculator">
      <div class="row">
        <div class="col-12">
          <h4>Pip Calculator</h4>
          <p class="description">To manage risk more effectively, it is important to know the pip value of each position in the currency of your trading account. The Pip Calculator does this for you. All you have to do is enter your position details, including the instrument you are trading, the trade size and your account currency. Click ‘Calculate’ and the Pip Calculator will determine how much each pip is worth.</p>
        </div>
      </div>
      <div class="row calculatorform">
        <div class="col-12 col-md-3">
          <label>Account Currency</label>
          <select class="accountcurrency" id="from">
            <option value="EUR" selected="selected">EUR</option>
            <option value="USD">USD</option>
          </select>
        </div>
        <div class="col-12 col-md-3">
          <label>Currency Pair</label>
          <select class="currencypair" id="to">
              <option value="CAD"> AUD/CAD</option>
              <option value="CHF"> AUD/CHF</option>
              <option value="JPY"> AUD/JPY</option>
              <option value="NZD"> AUD/NZD</option>
              <option value="USD"> AUD/USD</option>
              <option value="CHF"> CAD/CHF</option>
              <option value="JPY"> CAD/JPY</option>
              <option value="JPY"> CHF/JPY</option>
              <option value="AUD"> EUR/AUD</option>
              <option value="CAD"> EUR/CAD</option>
              <option value="CHF"> EUR/CHF</option>
              <option value="GBP"> EUR/GBP</option>
              <option value="JPY"> EUR/JPY</option>
              <option value="NZD"> EUR/NZD</option>
              <option value="USD"> EUR/USD</option>
              <option value="AUD"> GBP/AUD</option>
              <option value="CAD"> GBP/CAD</option>
              <option value="CHF"> GBP/CHF</option>
              <option value="JPY"> GBP/JPY</option>
              <option value="NZD"> GBP/NZD</option>
              <option value="USD"> GBP/USD</option>
              <option value="CAD"> NZD/CAD</option>
              <option value="CHF"> NZD/CHF</option>
              <option value="JPY"> NZD/JPY</option>
              <option value="USD"> NZD/USD</option>
              <option value="CAD"> USD/CAD</option>
              <option value="CHF"> USD/CHF</option>
              <option value="JPY"> USD/JPY</option>
          </select>
        </div>
        <div class="col-12 col-md-3">
          <label>Trade Size (In units):</label>
          <input type="number" value="10000" id="tradesize">
        </div>
        <div class="col-12 col-md-3">
          <label>Pip Value</label>
          <input type="text" value="0" id="to_amount" class="total" readonly="true">
        </div>
        <div class="col-12 col-md-3 current">
          <input class="btn-success" type="submit" value="Calculate" onclick="buyPriceAjax()">
        </div>
      </div>
    </div>
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
  </html>