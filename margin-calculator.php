<!DOCTYPE html>
  <html>
    <head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.js"></script>
	<script type="text/javascript">
		function buyPriceAjax(){
			var xhttp;
			xhttp = new XMLHttpRequest();

			var from = document.getElementById('from').value;
			var to = document.getElementById('to').value;
			var from_amount = document.getElementById('tradesize').value;
            var leverage = document.getElementById('leverage').value;

			xhttp.onreadystatechange = function() {
				if(this.readyState == 4 && this.status == 200){

          document.getElementById("to_amount").value = this.responseText;
				}
			};
			xhttp.open("GET", "includes/margin-helper.php?function=0&from="+from+"&to="+to+"&from_amount="+from_amount+"&leverage="+leverage, true);
			xhttp.send();
		}
	</script>
    </head>
    <body>
    <div class="container calculator">
      <div class="row">
        <div class="col-12">
          <h4>Margin Calculator</h4>
          <p class="description">Margin Calculator works out exactly how much margin is required in order to guarantee a position that you would like to open. This helps you determine whether you should reduce the lot size you are trading, or adjust the leverage you are using, taking into account your account balance. Select your trading instrument, your trade size, leverage and account currency, and click ‘Calculate’. Our Margin Calculator will do the rest.</p>
        </div>
      </div>
      <div class="row calculatorform">
        <div class="col-12 col-md-3">
          <label>Account Currency</label>
          <select class="accountcurrency" id="from">
            <option value="EUR" selected="selected">EUR</option>
            <option value="USD">USD</option>
          </select>
        </div>
        <div class="col-12 col-md-3">
          <label>Currency Pair</label>
          <select class="currencypair" id="to">
              <option value="AUD"> AUD/CAD</option>
              <option value="AUD"> AUD/CHF</option>
              <option value="AUD"> AUD/JPY</option>
              <option value="AUD"> AUD/NZD</option>
              <option value="AUD"> AUD/USD</option>
              <option value="CAD"> CAD/CHF</option>
              <option value="CAD"> CAD/JPY</option>
              <option value="CHF"> CHF/JPY</option>
              <option value="EUR"> EUR/AUD</option>
              <option value="EUR"> EUR/CAD</option>
              <option value="EUR"> EUR/CHF</option>
              <option value="EUR"> EUR/GBP</option>
              <option value="EUR"> EUR/JPY</option>
              <option value="EUR"> EUR/NZD</option>
              <option value="EUR"> EUR/USD</option>
              <option value="GBP"> GBP/AUD</option>
              <option value="GBP"> GBP/CAD</option>
              <option value="GBP"> GBP/CHF</option>
              <option value="GBP"> GBP/JPY</option>
              <option value="GBP"> GBP/NZD</option>
              <option value="GBP"> GBP/USD</option>
              <option value="NZD"> NZD/CAD</option>
              <option value="NZD"> NZD/CHF</option>
              <option value="NZD"> NZD/JPY</option>
              <option value="NZD"> NZD/USD</option>
              <option value="USD"> USD/CAD</option>
              <option value="USD">USD/CHF</option>
              <option value="USD">USD/JPY</option>
          </select>
        </div>
        <div class="col-12 col-md-3">
          <label>Trade Size (In units):</label>
          <input type="number" value="10000" id="tradesize">
        </div>
        <div class="col-12 col-md-3">
          <label>Leverage</label>
          <select id="leverage">
            <option value="30">1:30</option>
            <option value="20">1:20</option>
            <option value="10">1:10</option>
            <option value="5">1:5</option>
            <option value="2">1:2</option>
            <option value="1">1:1</option>
          </select>
        </div>
        <div class="col-12 col-md-3">
          <label>Required margin</label>
          <input type="text" value="0" id="to_amount" class="total" readonly="true">
        </div>
        <div class="col-12 col-md-3 current">
          <input class="btn-success" type="submit" value="Calculate" onclick="buyPriceAjax()">
        </div>
      </div>
    </div>
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
  </html>