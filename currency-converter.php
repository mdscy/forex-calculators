<!DOCTYPE html>
  <html>
    <head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script type="text/javascript">
		function buyPriceAjax(){
			var xhttp;
			xhttp = new XMLHttpRequest();

			var from = document.getElementById('from').value;
			var to = document.getElementById('to').value;
			var from_amount = document.getElementById('from_amount').value;

			xhttp.onreadystatechange = function() {
				if(this.readyState == 4 && this.status == 200){

          document.getElementById("to_amount").value = this.responseText;
				}
			};
			xhttp.open("GET", "includes/currency-helper.php?function=0&from="+from+"&to="+to+"&from_amount="+from_amount, true);
			xhttp.send();
		}
	</script>
    </head>
    <body>
    <div class="container calculator">
      <div class="row">
        <div class="col-12">
          <h4>Currency Converter</h4>
          <p class="description">Foreign Currency Converter helps you perform conversions on a range of currencies, quickly and easily, using live market rates. To use the Currency Converter, select your currency, the currency you would like to convert to, and the sum you would like converted. Finally, click ‘Calculate’ and the Currency Converter will perform the requested conversion.</p>
        </div>
      </div>
      <div class="row calculatorform">
        <div class="col-12 col-md-3">
          <label>Currency From</label>
          <select class="currencyfrom" id="from">
            <option value="AUD">AUD</option>
            <option value="CAD">CAD</option>
            <option value="CHF">CHF</option>
            <option value="CNH">CNH</option>
            <option value="DKK">DKK</option>
            <option value="EUR" selected="selected">EUR</option>
            <option value="GBP">GBP</option>
            <option value="HKD">HKD</option>
            <option value="JPY">JPY</option>
            <option value="MXN">MXN</option>
            <option value="NOK">NOK</option>
            <option value="NZD">NZD</option>
            <option value="PLN">PLN</option>
            <option value="RUB">RUB</option>
            <option value="SD">SD</option>
            <option value="SEK">SEK</option>
            <option value="SGD">SGD</option>
            <option value="TRY">TRY</option>
            <option value="USD">USD</option>
            <option value="ZAR">ZAR</option>
          </select>
        </div>
        <div class="col-12 col-md-3">
          <label>Currency To</label>
          <select class="currencyto" id="to">
            <option value="AUD">AUD</option>
            <option value="CAD">CAD</option>
            <option value="CHF">CHF</option>
            <option value="CNH">CNH</option>
            <option value="DKK">DKK</option>
            <option value="EUR">EUR</option>
            <option value="GBP">GBP</option>
            <option value="HKD">HKD</option>
            <option value="JPY">JPY</option>
            <option value="MXN">MXN</option>
            <option value="NOK">NOK</option>
            <option value="NZD">NZD</option>
            <option value="PLN">PLN</option>
            <option value="RUB">RUB</option>
            <option value="SD">SD</option>
            <option value="SEK">SEK</option>
            <option value="SGD">SGD</option>
            <option value="TRY">TRY</option>
            <option value="USD" selected="selected">USD</option>
            <option value="ZAR">ZAR</option>
          </select>
        </div>
        <div class="col-12 col-md-3">
          <label>Amount</label>
          <input type="number" value="10000" id="from_amount">
        </div>
        <div class="col-12 col-md-3">
          <label>Value</label>
          <input type="text" value="0" id="to_amount" class="total" readonly="true">
        </div>
        <div class="col-12 col-md-3 current">
          <input class="btn-success" type="submit" value="Convert" onclick="buyPriceAjax()">
        </div>
      </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
  </html>