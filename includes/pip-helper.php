<?php

define('URL', 'https://data.fixer.io/api');

if(isset($_GET['function'])){
    if(@$_GET['function']==0){
        $from_amount = $_GET['from_amount'];
        $to = $_GET['to'];
        $price = buyPrice($_GET['to'], $_GET['from']);
        $pip = "0.0001";
        if( $to == "JPY") {
            echo number_format($from_amount * $pip / $price * 100, 5);
        } else {
            echo number_format($from_amount * $pip / $price, 5);
        }
    }
}

function buyPrice($to, $from){
    $accesskey = '97e93b57f45bdc322b781d2a10692e09';
    $target_url = URL . "/latest?access_key=$accesskey" . "&symbols=$to&base=$from";
    $response = file_get_contents($target_url);
    return json_decode($response, true)["rates"][$to];
}
?>