<?php

define('URL', 'https://data.fixer.io/api');

if(isset($_GET['function'])){
    if(@$_GET['function']==0){
        $from_amount = $_GET['from_amount'];
        $leverage = $_GET['leverage'];
        $price = buyPrice($_GET['to'], $_GET['from']);
        echo number_format($from_amount / $leverage * $price, 2);
    }
}

function buyPrice($to, $from){
    $accesskey = '97e93b57f45bdc322b781d2a10692e09';
    $target_url = URL . "/latest?access_key=$accesskey" . "&symbols=$from&base=$to";
    $response = file_get_contents($target_url);
    return json_decode($response, true)["rates"][$from];
}
?>